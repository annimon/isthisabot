# [@IsThisABot](https://t.me/isthisabot)

Telegram inline bot that generates meme stickers

## Building

Build jar with dependencies: `./gradlew shadowJar`. The executable jar will appear as `build/libs/IsThisABot-all.jar`

## Running

```
java -Djava.library.path=lib \    # directory path contains libwebp-imageio.so
     -Dbot.token="XXXX:XXXXXX" \  # telegram bot token
     -Dbot.username=nameyourbot \ # telegram bot username
     -Dbot.channel="-100123456" \ # telegram channel id for publishing new stickers
     -jar IsThisABot-all.jar
```
