package com.annimon.itabot.services;

import com.annimon.itabot.StickerGenerators;

public interface GeneratorsLoaderService {

    StickerGenerators load();
}
