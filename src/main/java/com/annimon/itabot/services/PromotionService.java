package com.annimon.itabot.services;

import com.annimon.itabot.beans.Promotions;
import com.annimon.tgbotsmodule.api.methods.Methods;
import com.annimon.tgbotsmodule.exceptions.ConfigLoaderException;
import com.annimon.tgbotsmodule.services.CommonAbsSender;
import com.annimon.tgbotsmodule.services.YamlConfigLoaderService;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.message.Message;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.cached.InlineQueryResultCachedPhoto;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class PromotionService {

    public static final File PROMOTIONS = new File("config/promotions.yml");

    private final YamlConfigLoaderService promotionsLoader;
    private Promotions promotions;

    public PromotionService() {
        promotionsLoader = new YamlConfigLoaderService();
        if (PROMOTIONS.exists()) {
            this.promotions = loadFileOrDefault();
        } else {
            this.promotions = new Promotions();
        }
    }

    @NotNull
    public Promotions loadFile(@NotNull File file) {
        return promotionsLoader.loadFile(file, Promotions.class);
    }

    @NotNull
    private Promotions loadFileOrDefault() {
        try {
            return loadFile(PROMOTIONS);
        } catch (RuntimeException ex) {
            return new Promotions();
        }
    }

    public void promote(Message message, CommonAbsSender sender) {
        final var reply = message.getReplyToMessage();
        if (reply != null && reply.hasDocument()) {
            try {
                final var filePath = Methods.getFile(reply.getDocument().getFileId())
                        .call(sender)
                        .getFilePath();
                final File tmp = sender.downloadFile(filePath);
                promoteConfig(tmp, message.getChatId(), sender);
            } catch (TelegramApiException ex) {
                Methods.sendMessage(message.getChatId(), "Unable to set promotions: " + ex.getMessage())
                        .callAsync(sender);
            }
            return;
        }

        if (reply != null && reply.hasText()) {
            try {
                final File tmp = File.createTempFile("tmp_" + System.currentTimeMillis(), ".tmp");
                FileUtils.writeStringToFile(tmp, reply.getText(), StandardCharsets.UTF_8);
                promoteConfig(tmp, message.getChatId(), sender);
            } catch (IOException ex) {
                Methods.sendMessage(message.getChatId(), "Unable to set promotions")
                        .callAsync(sender);
            }
            return;
        }

        Methods.sendMessage(message.getChatId(), "Update promotions through reply!")
                .callAsync(sender);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void promoteConfig(File file, Long chatId, CommonAbsSender sender) {
        try {
            promotions = loadFile(file);
            PROMOTIONS.delete();
            FileUtils.moveFile(file, PROMOTIONS);
            Methods.sendMessage()
                    .setChatId(chatId)
                    .setText("Promotions set: " + getPromotionTitles())
                    .callAsync(sender);
        } catch (ConfigLoaderException | IOException ex) {
            Methods.sendMessage(chatId, "Unable to load config: " + ex.getMessage())
                    .callAsync(sender);
        }
    }

    private String getPromotionTitles() {
        return promotions.promotions.stream()
                .map(p -> p.title)
                .collect(Collectors.joining(", "));
    }

    public void fillPromotions(String locale, List<InlineQueryResult> results) {
        var list = promotions.promotions.stream()
                .filter(p -> (p.lang == null) || (p.lang.equalsIgnoreCase(locale)))
                .map(promotion -> {
                    if (promotion.photoId != null) {
                        return InlineQueryResultCachedPhoto.builder()
                                .id(Integer.toString(promotion.hashCode()))
                                .title(promotion.title)
                                .photoFileId(promotion.photoId)
                                .caption(Optional.ofNullable(promotion.content).orElse(""))
                                .parseMode(ParseMode.MARKDOWN)
                                .build();
                    } else {
                        return InlineQueryResultArticle.builder()
                                .id(Integer.toString(promotion.hashCode()))
                                .title(promotion.content)
                                .inputMessageContent(InputTextMessageContent.builder()
                                        .messageText(promotion.content)
                                        .parseMode(ParseMode.MARKDOWN)
                                        .build())
                                .build();
                    }
                })
                .toList();
        results.addAll(list);
    }
}
