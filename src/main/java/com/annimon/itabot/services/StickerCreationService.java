package com.annimon.itabot.services;

import com.annimon.itabot.beans.StickerCreationStatus;
import java.io.File;
import java.util.function.Function;
import org.telegram.telegrambots.meta.api.objects.message.Message;
import org.telegram.telegrambots.meta.api.objects.User;

public interface StickerCreationService {

    StickerCreationStatus createSticker(String query, User user, Function<File, Message> sendStickerAction);
}
