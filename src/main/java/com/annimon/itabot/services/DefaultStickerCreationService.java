package com.annimon.itabot.services;

import com.annimon.itabot.StickerGenerators;
import com.annimon.itabot.beans.StickerCreationStatus;
import com.annimon.itabot.generators.StickerGenerator;
import java.io.File;
import java.util.Optional;
import java.util.function.Function;
import org.telegram.telegrambots.meta.api.objects.message.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.stickers.Sticker;

public class DefaultStickerCreationService implements StickerCreationService {

    private final StickerGenerators stickerGenerators;
    private final StickersCacheService stickersCache;

    public DefaultStickerCreationService(StickerGenerators generators, StickersCacheService cache) {
        this.stickerGenerators = generators;
        this.stickersCache = cache;
    }

    @Override
    public StickerCreationStatus createSticker(String query, User user, Function<File, Message> sendStickerAction) {
        if (query == null || query.isBlank()) {
            return StickerCreationStatus.FAIL;
        }
        if (!query.endsWith(".")) {
            return StickerCreationStatus.NEED_DOT;
        }
        query = query.substring(0, query.length() - 1);

        final var status = stickerGenerators.preprocess(query);
        if (status.isFail()) {
            return StickerCreationStatus.FAIL;
        }

        final var generator = status.getGenerator();
        final String caption = status.getCaption();

        final String key = query;
        String fileId = stickersCache.get(key);
        if (fileId == null) {
            var result = generateSticker(generator, caption, sendStickerAction);
            if (result.isEmpty()) {
                return StickerCreationStatus.FAIL;
            }
            fileId = result.get();
            stickersCache.put(key, fileId);
            return StickerCreationStatus.with(fileId, caption, false);
        }
        return StickerCreationStatus.with(fileId, caption, true);
    }

    private Optional<String> generateSticker(StickerGenerator stickerGenerator, String caption,
                                             Function<File, Message> sendStickerAction) {
        final var tmpWebpFile = stickerGenerator.generate(caption);
        if (tmpWebpFile == null) return Optional.empty();

        try {
            final Message msg = sendStickerAction.apply(tmpWebpFile);
            return Optional.ofNullable(msg)
                    .map(Message::getSticker)
                    .map(Sticker::getFileId);
        } finally {
            tmpWebpFile.delete();
        }
    }
}
