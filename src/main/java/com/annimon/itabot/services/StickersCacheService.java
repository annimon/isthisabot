package com.annimon.itabot.services;

public interface StickersCacheService {

    String get(String key);

    String put(String key, String value);
}
