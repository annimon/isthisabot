package com.annimon.itabot.services;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruStickersCacheService implements StickersCacheService {

    private final Map<String, String> stickersCache;

    public LruStickersCacheService(final int maxEntries) {
        stickersCache = new LinkedHashMap<>(maxEntries + 1, 0.75f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<String, String> eldest) {
                return size() > maxEntries;
            }
        };
    }

    @Override
    public String get(String key) {
        return stickersCache.get(key);
    }

    @Override
    public String put(String key, String value) {
        return stickersCache.put(key, value);
    }
}
