package com.annimon.itabot.services;

import com.annimon.itabot.StickerGenerators;
import com.annimon.itabot.beans.GeneratorConfig;
import com.annimon.itabot.generators.StickerGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigGeneratorsLoaderService implements GeneratorsLoaderService {

    private static final Logger log = LoggerFactory.getLogger(ConfigGeneratorsLoaderService.class);

    private final GeneratorConfig[] generatorConfigs;

    public ConfigGeneratorsLoaderService(GeneratorConfig[] generatorConfigs) {
        this.generatorConfigs = generatorConfigs;
    }

    @Override
    public StickerGenerators load() {
        final var generators = new StickerGenerators();
        for (var config : generatorConfigs) {
            final StickerGenerator generator = forName(config.getClassName());
            if (generator == null) continue;

            generators.add(generator, config.getAliases());
        }
        return generators;
    }

    private StickerGenerator forName(String className) {
        try {
            return (StickerGenerator) Class
                    .forName(className)
                    .getDeclaredConstructor()
                    .newInstance();
        } catch (Exception ex) {
            log.warn(className + " not exists", ex);
            return null;
        }
    }
}
