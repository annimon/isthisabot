package com.annimon.itabot.services;

import com.annimon.tgbotsmodule.analytics.Event;
import com.annimon.tgbotsmodule.analytics.EventRegister;

public class EmptyEventRegister implements EventRegister {

    @Override
    public void received(Event event) {

    }

    @Override
    public void sent(Event event) {

    }
}
