package com.annimon.itabot.services;

import com.annimon.itabot.StickerGenerators;
import com.annimon.itabot.generators.StickerGenerator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

@Deprecated
public class SimpleGeneratorsLoaderService implements GeneratorsLoaderService {

    private static final String LOGTAG = SimpleGeneratorsLoaderService.class.getName();

    private final String resource;

    public SimpleGeneratorsLoaderService(String resource) {
        this.resource = resource;
    }

    @Override
    public StickerGenerators load() {
        final var generators = new StickerGenerators();
        try (var is = getClass().getResourceAsStream(resource);
             var reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                final String[] args = line.split("\\|", 2);
                if (args.length != 2) continue;

                final var generator = forName(args[0]);
                if (generator == null) continue;

                generators.add(generator, line, args[1].split(","));
            }
        } catch (IOException ioe) {
            Logger.getLogger(LOGTAG).log(Level.WARNING, "unable to load resource " + resource, ioe);
        }
        return generators;
    }

    private StickerGenerator forName(String className) {
        try {
            return (StickerGenerator) Class
                    .forName(className)
                    .getDeclaredConstructor()
                    .newInstance();
        } catch (Exception ex) {
            Logger.getLogger(LOGTAG).log(Level.WARNING, className + " not exists", ex);
            return null;
        }
    }
}
