package com.annimon.itabot.services;

public interface LocalizationService {

    String getString(String key, String language);
}
