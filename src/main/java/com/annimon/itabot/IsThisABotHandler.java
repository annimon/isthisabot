package com.annimon.itabot;

import com.annimon.common.AdminCommandService;
import com.annimon.itabot.beans.ItaBotConfig;
import com.annimon.itabot.services.LocalizationService;
import com.annimon.itabot.services.PromotionService;
import com.annimon.itabot.services.StickerCreationService;
import com.annimon.tgbotsmodule.BotHandler;
import com.annimon.tgbotsmodule.BotModuleOptions;
import com.annimon.tgbotsmodule.analytics.UpdateHandler;
import com.annimon.tgbotsmodule.api.methods.Methods;
import com.annimon.tgbotsmodule.commands.authority.SimpleAuthority;
import java.util.ArrayList;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.botapimethods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.message.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultsButton;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.cached.InlineQueryResultCachedSticker;

public class IsThisABotHandler extends BotHandler {

    private final ItaBotConfig botConfig;
    private final StickerGenerators stickerGenerators;
    private final LocalizationService localization;
    private final StickerCreationService stickerCreation;
    private final UpdateHandler adminCommandService;
    private final SimpleAuthority authority;
    private final PromotionService promotionService;

    public IsThisABotHandler(BotModuleOptions botModuleOptions,
                             ItaBotConfig botConfig, StickerGenerators sg,
                             LocalizationService loc, StickerCreationService sc) {
        super(botModuleOptions);
        this.botConfig = botConfig;
        this.stickerGenerators = sg;
        this.localization = loc;
        this.stickerCreation = sc;
        promotionService = new PromotionService();
        authority = new SimpleAuthority(botConfig.getCreatorId());
        adminCommandService = new AdminCommandService(this, authority);
    }

    @Override
    public BotApiMethod<?> onUpdate(@NotNull Update update) {
        if (adminCommandService.handleUpdate(this, update)) {
            return null;
        }

        if (update.hasInlineQuery()) {
            handleInlineQuery(update.getInlineQuery());
            return null;
        }

        final var message = update.getMessage();
        if (message != null && message.hasText() && message.isUserMessage()) {
            handleMessage(message);
            return null;
        }
        return null;
    }

    private void handleMessage(Message message) {
        final var args = message.getText().split("\\s+", 2);
        final String command = args[0].replace(getBotUsername(), "");
        final String rest = args.length == 2 ? args[1] : "";
        // Admin actions
        if (authority.getCreatorId() == message.getFrom().getId()) {
            switch (command) {
                case "/id", "/json" -> {
                    Methods.sendMessage()
                            .setChatId(message.getChatId())
                            .setText(message.toString())
                            .callAsync(this);
                }
                case "/promote", "/promotions" -> {
                    promotionService.promote(message, this);
                }
                case "/privacy" -> {
                    Methods.sendMessage()
                            .setChatId(message.getChatId())
                            .setText("https://t.me/isthisachannel/48")
                            .callAsync(this);
                }
            }
        }

        switch (command) {
            case "/start" -> handleStart(message);
            case "/start list", "/help" -> handleHelp(message);
            default -> handleOtherMessage(message);
        }
    }

    private void handleStart(Message message) {
        final String locale = getUserLocale(message.getFrom());
        final String text = localization.getString("start", locale);
        sendDescriptionMessage(message.getChatId(), text);
    }

    private void handleHelp(Message message) {
        final String locale = getUserLocale(message.getFrom());
        sendDescriptionMessage(message.getChatId(), localization.getString("help", locale));
    }

    private void sendDescriptionMessage(Long chatId, String text) {
        if (text == null || text.isEmpty()) return;

        Methods.sendMessage()
                .setChatId(chatId)
                .enableMarkdown(true)
                .setText(text.replace("%BOTNAME%", getBotUsername()))
                .callAsync(this);
    }

    private void handleOtherMessage(final Message message) {
        String query = message.getText();
        if (!query.endsWith(".")) {
            query += ".";
        }
        final var status = stickerCreation
                .createSticker(query, message.getFrom(), file ->
                        Methods.Stickers.sendSticker()
                                .setChatId(message.getChatId())
                                .setFile(file)
                                .call(this));
        if (!status.isFail() && status.isUsedCache()) {
            // Sticker already exists in cache, send as fileId
            Methods.Stickers.sendSticker()
                    .setChatId(message.getChatId())
                    .setFile(status.getFileId())
                    .callAsync(this);
        }
    }

    private void handleInlineQuery(final InlineQuery inlineQuery) {
        if (inlineQuery == null || inlineQuery.getQuery() == null) return;

        final User from = inlineQuery.getFrom();
        final var query = inlineQuery.getQuery();

        final var status = stickerCreation
                .createSticker(query, from, file ->
                        Methods.Stickers.sendSticker()
                                .setChatId(botConfig.getChannel())
                                .setFile(file)
                                .call(this));
        final String locale = getUserLocale(from);
        final var results = new ArrayList<InlineQueryResult>(5);
        if (!status.isFail()) {
            final var ir = InlineQueryResultCachedSticker.builder()
                    .id(Integer.toString(status.getCaption().hashCode()))
                    .stickerFileId(status.getFileId())
                    .build();
            results.add(ir);
        } else if (status.isNeedDot()) {
            final var imc = InputTextMessageContent.builder()
                    .messageText(localization.getString("needdot_description", locale)
                            .replace("%BOTNAME%", getBotUsername()))
                    .parseMode(ParseMode.MARKDOWN)
                    .build();

            final var ir = InlineQueryResultArticle.builder()
                    .id("need_dot")
                    .title(localization.getString("needdot", locale))
                    .inputMessageContent(imc)
                    .build();
            results.add(ir);
        }
        promotionService.fillPromotions(locale, results);
        Methods.answerInlineQuery()
                .setInlineQueryId(inlineQuery.getId())
                .setButton(InlineQueryResultsButton.builder()
                        .startParameter("list")
                        .text(localization.getString("more", locale))
                        .build())
                .setResults(results)
                .callAsync(this);
    }

    private String getUserLocale(User user) {
        return Optional.ofNullable(user)
                .map(User::getLanguageCode)
                .orElse("en");
    }

    private String getBotUsername() {
        return botConfig.getUsername();
    }
}
