package com.annimon.itabot;

import com.annimon.itabot.beans.StickerPreprocessStatus;
import com.annimon.itabot.exceptions.EmptyCaptionException;
import com.annimon.itabot.generators.StickerGenerator;
import java.util.HashMap;
import java.util.Map;

public class StickerGenerators {

    private final Map<String, StickerGenerator> generators;
    private StickerGenerator defaultStickerGenerator;

    public StickerGenerators() {
        generators = new HashMap<>();
    }

    public void setDefaultStickerGenerator(StickerGenerator defaultStickerGenerator) {
        this.defaultStickerGenerator = defaultStickerGenerator;
    }

    public boolean contains(String key) {
        if (key.isEmpty()) return false;
        return generators.containsKey(key);
    }

    public StickerGenerator get(String key) {
        return generators.get(key);
    }

    public StickerGenerators add(StickerGenerator generator, String[] keys) {
        for (String key : keys) {
            generators.put(key, generator);
        }
        return this;
    }

    public StickerGenerators add(StickerGenerator generator, String key1, String... restKeys) {
        generators.put(key1, generator);
        for (String key : restKeys) {
            generators.put(key, generator);
        }
        return this;
    }

    public StickerPreprocessStatus preprocess(String query) {
        if (query == null || query.isEmpty()) {
            return StickerPreprocessStatus.FAIL;
        }

        final String generatorId;
        final int separatorPos = query.indexOf(',');
        if (separatorPos > 0) {
            generatorId = query.substring(0, separatorPos).toLowerCase();
        } else {
            generatorId = "";
        }

        try {
            final String caption;
            final StickerGenerator generator;
            if (contains(generatorId) && query.length() > (separatorPos + 1)) {
                generator = get(generatorId);
                caption = generator.processCaption(generatorId, query.substring(separatorPos + 1).trim());
            } else {
                generator = defaultStickerGenerator;
                caption = generator.processCaption(generatorId, query);
            }
            return new StickerPreprocessStatus(false, generator, caption);
        } catch (EmptyCaptionException ex) {
            return StickerPreprocessStatus.FAIL;
        }
    }
}
