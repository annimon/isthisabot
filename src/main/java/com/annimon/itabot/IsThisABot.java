package com.annimon.itabot;

import com.annimon.itabot.beans.ItaBotConfig;
import com.annimon.itabot.beans.GeneratorConfig;
import com.annimon.itabot.beans.StickerPreprocessStatus;
import com.annimon.itabot.generators.IsThisAPigeonStickerGenerator;
import com.annimon.itabot.services.ConfigGeneratorsLoaderService;
import com.annimon.itabot.services.DefaultStickerCreationService;
import com.annimon.itabot.services.LruStickersCacheService;
import com.annimon.itabot.services.ResourceBundleLocalizationService;
import com.annimon.tgbotsmodule.BotHandler;
import com.annimon.tgbotsmodule.BotModule;
import com.annimon.tgbotsmodule.BotModuleOptions;
import com.annimon.tgbotsmodule.Runner;
import com.annimon.tgbotsmodule.beans.Config;
import com.annimon.tgbotsmodule.services.YamlConfigLoaderService;
import java.io.File;
import java.util.List;
import java.util.function.Consumer;
import javax.swing.*;

public class IsThisABot implements BotModule {

    public static void main(String[] args) {
        if (args.length >= 2 && args[0].equalsIgnoreCase("generate")) {
            generateSticker(loadGenerators(), args[1]);
            return;
        }
        if (args.length >= 2 && args[0].equalsIgnoreCase("preview")) {
            previewSticker(loadGenerators(), args[1]);
            return;
        }
        final var profile = (args.length >= 1 && !args[0].isEmpty()) ? args[0] : "";
        Runner.run(profile, List.of(new IsThisABot()));
    }

    @Override
    public BotHandler botHandler(Config config) {
        final var itaBotConfig = loadConfig(config.getProfile());
        final var stickerGenerators = newStickerGenerators(loadGenerators());
        final var localization = new ResourceBundleLocalizationService("Language");

        final int MAX_CACHE_ENTRIES = 500;
        final var cache = new LruStickersCacheService(MAX_CACHE_ENTRIES);
        final var sc = new DefaultStickerCreationService(stickerGenerators, cache);

        final var botModuleOptions = BotModuleOptions.createDefault(itaBotConfig.getToken());
        return new IsThisABotHandler(botModuleOptions, itaBotConfig, stickerGenerators, localization, sc);
    }

    private static GeneratorConfig[] loadGenerators() {
        final var configLoader = new YamlConfigLoaderService();
        return configLoader.loadResource("/generators.yml", GeneratorConfig[].class);
    }

    private static ItaBotConfig loadConfig(String profile) {
        final var configLoader = new YamlConfigLoaderService();
        final var configFile = configLoader.configFile("config/itabot", profile);
        return configLoader.loadFile(configFile, ItaBotConfig.class);
    }

    private static StickerGenerators newStickerGenerators(GeneratorConfig[] generators) {
        final var loaderService = new ConfigGeneratorsLoaderService(generators);
        final var stickerGenerators = loaderService.load();
        stickerGenerators.setDefaultStickerGenerator(new IsThisAPigeonStickerGenerator());
        return stickerGenerators;
    }

    private static void generateSticker(GeneratorConfig[] generators, String query) {
        stickerAction(generators, query, (status) -> {
            final var generator = status.getGenerator();
            final String caption = status.getCaption();
            final File sticker = generator.generate(caption);
            System.out.println(sticker.getAbsolutePath());
        });
    }

    private static void previewSticker(GeneratorConfig[] generators, String query) {
        stickerAction(generators, query, (status) -> {
            final var generator = status.getGenerator();
            final String caption = status.getCaption();
            final var sticker = generator.generateImage(caption);
            final var frame = new JFrame("Preview sticker");
            final var icon = new ImageIcon(sticker);
            frame.setContentPane(new JLabel(icon));
            frame.pack();
            frame.setLocationByPlatform(true);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }

    private static void stickerAction(GeneratorConfig[] generators, String query,
                                      Consumer<StickerPreprocessStatus> preprocessStatusConsumer) {
        final var stickerGenerators = newStickerGenerators(generators);
        final var status = stickerGenerators.preprocess(query);
        if (status.isFail()) {
            throw new IllegalStateException("Cannot generate sticker");
        } else {
            preprocessStatusConsumer.accept(status);
        }
    }
}
