package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class ThreeAMStickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/3am.png";
    }

    @Override
    protected int actualStickerWidth() {
        return 496;
    }

    @Override
    protected int stickerHeight() {
        return 496;
    }

    @Override
    protected int minFontSize() {
        return super.minFontSize();
    }

    @Override
    protected int maxFontSize() {
        return 60;
    }

    @Override
    protected int textBoundsBorderSize() {
        return 0;
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 0f;
    }

    @Override
    protected Color textColor() {
        return Color.LIGHT_GRAY;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f + width / 6f - (bounds.width / 2f),
                height / 2f - 8 - (bounds.height / 2f));
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return super.detectBestFont(frc, caption, maxWidth - maxWidth / 3);
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim();
    }
}
