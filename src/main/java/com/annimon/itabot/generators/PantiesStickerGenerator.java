package com.annimon.itabot.generators;

import com.annimon.itabot.exceptions.EmptyCaptionException;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.regex.Pattern;
import org.jetbrains.annotations.Nullable;

public class PantiesStickerGenerator extends TwoCaptionsStickerGenerator {

    private static final float DEFAULT_HUE = 0.505f;
    private static final Pattern HUE_PATERN = Pattern.compile("(.*)\\, ?(\\d?\\d)$");
    
    private float hue = DEFAULT_HUE;

    @Override
    protected String templateResource() {
        return "/panties.png";
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 25 - (bounds.height / 2f));
    }

    @Override
    protected void transformSecondaryCaption(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), 65 + (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        super.setId(id);
        final var matcher = HUE_PATERN.matcher(caption.trim());
        if (!matcher.find()) {
            hue = DEFAULT_HUE;
            return caption.trim().toUpperCase();
        } else {
            final String newCaption = matcher.group(1);
            if (newCaption.isEmpty()) throw new EmptyCaptionException();
            hue = Integer.parseInt(matcher.group(2)) / 100f;
            return newCaption.toUpperCase();
        }
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        if (id == null) {
            return "ПОДАРОК";
        }
        return id.toUpperCase();
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        return 68;
    }

    @Override
    protected void preImageRender(Graphics2D g, int width, int height) {
        g.setColor(Color.getHSBColor(hue, 0.7f, 0.81f));
        g.fillRect(0, 0, width, height);
    }

    @Override
    protected Color strokeColor() {
        return Color.BLACK;
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }

    @Override
    protected int maxFontSize() {
        return 64;
    }
}
