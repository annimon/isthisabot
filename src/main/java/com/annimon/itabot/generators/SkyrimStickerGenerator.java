package com.annimon.itabot.generators;

import com.annimon.itabot.exceptions.EmptyCaptionException;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.regex.Pattern;
import org.jetbrains.annotations.Nullable;

public class SkyrimStickerGenerator extends TwoCaptionsStickerGenerator {

    private static final int GAP = 30;
    private static final int DEFAULT_LEVEL = 100;
    private static final Pattern LEVEL_PATERN = Pattern.compile("(.*), ?(\\d{1,3})$");
    
    private int level = DEFAULT_LEVEL;
    private BufferedImage frame;

    private float lastCaptionWidth = 0f, lastCaptionHeight = 0f;
    private int captionLength = 0;

    @Override
    protected String templateResource() {
        return "/skyrim.png";
    }

    private BufferedImage frameImage() {
        if (frame == null) {
            frame = loadImage("/skyrim-frame.png").orElseThrow();
        }
        return frame;
    }

    @Override
    protected int stickerHeight() {
        return 170;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        // level caption max width + gap
        lastCaptionWidth = bounds.width;
        lastCaptionHeight = bounds.height;
        transform.translate(
                (width - bounds.width - maxLevelWidth()) / 2f,
                height - 50 - bounds.height / 2f);
    }

    @Override
    protected void transformSecondaryCaption(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(
                (width - lastCaptionWidth - maxLevelWidth()) / 2f + lastCaptionWidth + GAP,
                height - 50 - lastCaptionHeight / 2f);
    }

    private int maxLevelWidth() {
        final int digitsCount;
        if (level == 0) {
            digitsCount = 1;
        } else {
            digitsCount = (int) (Math.log10(level) + 1);
        }

        final int digitWidth;
        if (captionLength < 14) {
            digitWidth = 46;
        } else if (captionLength < 25) {
            digitWidth = 42;
        } else {
            digitWidth = 36;
        }

        return digitsCount * digitWidth + GAP;
    }

    @Override
    public String processCaption(String id, String caption) {
        super.setId(id);
        final var matcher = LEVEL_PATERN.matcher(caption.trim());
        if (!matcher.find()) {
            level = DEFAULT_LEVEL;
            final String result = caption.trim().toUpperCase();
            captionLength = result.length();
            return result;
        } else {
            final String newCaption = matcher.group(1);
            if (newCaption.isEmpty()) throw new EmptyCaptionException();
            level = Integer.parseInt(matcher.group(2));
            if (level > 100 && level != 146) level = 100;
            else if (level < 0) level = 0;
            captionLength = newCaption.length();
            return newCaption.toUpperCase();
        }
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        return Integer.toString(level);
    }

    @Override
    protected void postImageRender(Graphics2D g, int width, int height) {
        // Level bar
        frame = frameImage();
        final int barWidth = (Math.min(level, 100) * 330 / 100);
        g.setColor(level == 146 ? Color.RED : Color.LIGHT_GRAY);
        g.fillRect(91, 136, barWidth, 8);
        // Frame
        g.drawImage(frame, 0, 0, null);
        super.postImageRender(g, width, height);
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        if (captionLength < 14) {
            return 90;
        } else if (captionLength < 25) {
            return 80;
        } else {
            return 70;
        }
    }

    @Override
    protected Color strokeColor() {
        return Color.BLACK;
    }

    @Override
    protected Color textColor() {
        return Color.GRAY;
    }

    @Override
    protected Color secondaryTextColor() {
        return Color.WHITE;
    }

    @Override
    protected int maxFontSize() {
        return 70;
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        final int borderWidth = 10;
        return super.detectBestFont(frc, caption, 512 - maxLevelWidth() - 2 * borderWidth);
    }

    @Override
    protected Font fontBySize(int size) {
        return new Font("Roboto Condensed", Font.BOLD, size);
    }
}
