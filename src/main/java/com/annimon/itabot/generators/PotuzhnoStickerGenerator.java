package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.regex.Pattern;

public class PotuzhnoStickerGenerator extends AbstractStickerGenerator {

    private static final int DEFAULT_LEVEL = 100;
    private static final Pattern LEVEL_PATTERN = Pattern.compile("(\\d{1,3})$");
    
    private int level = DEFAULT_LEVEL;
    private BufferedImage indicator;

    @Override
    protected String templateResource() {
        return "/potuzhn1.png";
    }

    private BufferedImage indicatorImage() {
        if (indicator == null) {
            indicator = loadImage("/potuzhn2.png").orElseThrow();
        }
        return indicator;
    }

    @Override
    protected int stickerWidth() {
        return 352;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), 119);
    }

    @Override
    public String processCaption(String id, String caption) {
        final var matcher = LEVEL_PATTERN.matcher(caption.trim());
        if (!matcher.find()) {
            level = DEFAULT_LEVEL;
        } else {
            level = Math.min(100, Integer.parseInt(matcher.group(1)));
        }
        return level + "%";
    }

    @Override
    protected void postImageRender(Graphics2D g, int width, int height) {
        indicator = indicatorImage();
        final int iw = indicator.getWidth();
        final int ih = indicator.getHeight();
        final int indicatorLevel = (level * iw / 100);
        g.drawImage(indicator,
                70, 87, 70 + indicatorLevel, 87 + ih,
                0, 0, indicatorLevel, ih, null);
        super.postImageRender(g, width, height);
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 3f;
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return fontBySize(22);
    }
}
