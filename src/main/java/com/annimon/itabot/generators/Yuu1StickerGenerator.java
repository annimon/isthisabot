package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class Yuu1StickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/yuu1.jpg";
    }

    @Override
    protected int stickerWidth() {
        return 456;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(280 - (bounds.width / 2f), 470 - (bounds.height / 2.2f));
        transform.rotate(-0.33);
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected int minFontSize() {
        return 25;
    }

    @Override
    protected int maxFontSize() {
        return 50;
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return super.detectBestFont(frc, caption, 280);
    }
}
