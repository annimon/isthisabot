package com.annimon.itabot.generators;

import java.awt.image.BufferedImage;
import java.io.File;

public interface StickerGenerator {

    String processCaption(String id, String caption);

    File generate(String caption);

    BufferedImage generateImage(String caption);
}
