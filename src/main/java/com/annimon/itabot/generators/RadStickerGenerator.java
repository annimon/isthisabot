package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class RadStickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/rad.png";
    }

    @Override
    protected int stickerHeight() {
        return 250;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(28, 72 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        final String result = caption.trim();
        if (result.endsWith("?")) {
            return result;
        } else {
            return result + "?";
        }
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 0;
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }

    @Override
    protected int minFontSize() {
        return 12;
    }

    @Override
    protected int maxFontSize() {
        return 24;
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return super.detectBestFont(frc, caption, 400);
    }
}
