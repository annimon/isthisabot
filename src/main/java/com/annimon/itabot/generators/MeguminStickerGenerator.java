package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class MeguminStickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/megumin.png";
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 20 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected int textBoundsBorderSize() {
        return 15;
    }
}
