package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;
import org.jetbrains.annotations.Nullable;

public class NeedStickerGenerator extends TwoCaptionsStickerGenerator {

    @Override
    protected String templateResource() {
        return "/need.png";
    }

    @Override
    protected int stickerWidth() {
        return 512;
    }

    @Override
    protected int stickerHeight() {
        return 400;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 20 - (bounds.height / 2f));
    }

    @Override
    protected void transformSecondaryCaption(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), 65 + (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        super.setId(id);
        return caption.trim().toUpperCase();
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        if (id == null || id.equalsIgnoreCase("нужно")) {
            return "НУЖНО БЫЛО";
        }
        if (id.equalsIgnoreCase("need")) {
            return "NEED TO";
        }
        return id.toUpperCase();
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        return 54;
    }

    @Override
    protected Color strokeColor() {
        return Color.BLACK;
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }

    @Override
    protected int minFontSize() {
        return 25;
    }

    @Override
    protected int maxFontSize() {
        return 64;
    }
}
