package com.annimon.itabot.generators;

import java.awt.*;

public class CheemsStickerGenerator extends MultilineStickerGenerator {

    // private final Color TEXT_BG_COLOR = new Color(0x7FFFFFFF, true);

    protected String templateResource() {
        return "/cheems.png";
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected Rectangle bounds() {
        final int pad = 20;
        final int startY = 250;
        return new Rectangle(pad, startY, 512 - pad * 2, 512 - startY - pad);
    }

    @Override
    protected VerticalAlign verticalAlign() {
        return VerticalAlign.BOTTOM;
    }

    // not used, buggy
    /*@Override
    protected void preDrawText(Graphics2D g, float textHeight) {
        g.setColor(TEXT_BG_COLOR);
        final var b = bounds();
        b.grow(5, 5);
        final int th = (int) textHeight + 15;
        g.fillRect(b.x, b.y + b.height - th, b.width, th);
    }*/
}
