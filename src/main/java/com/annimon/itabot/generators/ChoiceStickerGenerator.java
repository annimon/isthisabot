package com.annimon.itabot.generators;

import com.annimon.itabot.exceptions.EmptyCaptionException;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.regex.Pattern;
import org.jetbrains.annotations.Nullable;

public class ChoiceStickerGenerator extends TwoCaptionsStickerGenerator {

    private static final Pattern CHOICES_PATERN = Pattern.compile("(.*)[,|] ?(.*)$");
    
    @Override
    protected String templateResource() {
        return "/choice.png";
    }

    @Override
    protected int stickerWidth() {
        return 370;
    }

    @Override
    protected int stickerHeight() {
        return 512;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 3f - 26 - (bounds.width / 2f), 100 + (bounds.height / 2f));
        transform.rotate(Math.toRadians(-15));
    }

    @Override
    protected void transformSecondaryCaption(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width - width / 3f - 26 - (bounds.width / 2f), 70 + (bounds.height / 2f));
        transform.rotate(Math.toRadians(-13));
    }

    @Override
    public String processCaption(String id, String caption) {
        final var matcher = CHOICES_PATERN.matcher(caption.trim());
        if (!matcher.find()) {
            throw new EmptyCaptionException();
        }

        final String firstCaption = matcher.group(1);
        if (firstCaption.isEmpty())
            throw new EmptyCaptionException();
        final String secondCaption = matcher.group(2);
        if (secondCaption.isEmpty())
            throw new EmptyCaptionException();

        super.setId(secondCaption.toUpperCase());
        return firstCaption.toUpperCase();
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        return id;
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        return super.detectBestFont(g.getFontRenderContext(), getId(), 90).getSize2D();
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 0;
    }

    @Override
    protected int minFontSize() {
        return 8;
    }

    @Override
    protected int maxFontSize() {
        return 40;
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return super.detectBestFont(frc, caption, 120);
    }
}
