package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractStickerGenerator implements StickerGenerator {

    private static final Logger log = LoggerFactory.getLogger(AbstractStickerGenerator.class);

    private BufferedImage stickerImage;

    @Override
    public final File generate(String caption) {
        File tmpWebpFile = null;
        try {
            tmpWebpFile = File.createTempFile("sticker", ".webp");
            ImageIO.write(generateImage(caption), "webp", tmpWebpFile);
        } catch (IOException ex) {
            log.error("abstract sticker generate", ex);
        }
        return tmpWebpFile;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public BufferedImage generateImage(String caption) {
        final int width = actualStickerWidth();
        final int height = actualStickerHeight();
        final var img = new BufferedImage(stickerWidth(), stickerHeight(), BufferedImage.TYPE_INT_ARGB);
        final var g = img.createGraphics();

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        final var frc = g.getFontRenderContext();
        final var font = detectBestFont(frc, caption, width - 2 * textBoundsBorderSize());

        final var textLayout = new TextLayout(caption, font, frc);
        final var transform = new AffineTransform();
        final Shape outline = textLayout.getOutline(null);
        final Rectangle outlineBounds = outline.getBounds();
        transform(width, height, transform, outlineBounds);
        preImageRender(g, width, height);
        g.drawImage(templateImage(), 0, 0, null);
        postImageRender(g, width, height);
        g.transform(transform);
        final float strokeSize = strokeSizeByFontSize(font.getSize());
        if (strokeSize > 0) {
            final var oldStroke = g.getStroke();
            g.setColor(strokeColor());
            g.setStroke(new BasicStroke(strokeSize));
            g.draw(outline);
            g.setStroke(oldStroke);
        }
        g.setColor(textColor());
        textLayout.draw(g, 0, 0);
        return img;
    }

    protected int stickerWidth() {
        return 512;
    }

    protected int stickerHeight() {
        return 512;
    }

    protected int actualStickerWidth() {
        return stickerWidth();
    }

    protected int actualStickerHeight() {
        return stickerHeight();
    }

    protected abstract String templateResource();

    protected Font fontBySize(int size) {
        return new Font("DejaVu Sans", Font.BOLD, size);
    }

    protected int minFontSize() {
        return 15;
    }

    protected int maxFontSize() {
        return 80;
    }

    protected int textBoundsBorderSize() {
        return 10;
    }

    protected float strokeSizeByFontSize(float fontSize) {
        return fontSize * 8f / 60f;
    }

    protected Color textColor() {
        return Color.BLACK;
    }

    protected Color strokeColor() {
        return Color.WHITE;
    }

    protected abstract void transform(final int width, final int height, AffineTransform transform, Rectangle bounds);


    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        Font font;
        TextLayout textLayout;
        int size = maxFontSize();
        do {
            font = fontBySize(size);
            textLayout = new TextLayout(caption, font, frc);
            size -= 1;
        } while (size > minFontSize() && textLayout.getBounds().getWidth() > maxWidth);
        return font;
    }

    protected BufferedImage templateImage() {
        if (stickerImage == null) {
            stickerImage = loadImage(templateResource()).orElseThrow();
        }
        return stickerImage;
    }

    protected Optional<BufferedImage> loadImage(String resource) {
        try (var is = getClass().getResource(resource).openStream()) {
            return Optional.of(ImageIO.read(is));
        } catch (IOException ex) {
            log.error("abstract sticker loadImage", ex);
            return Optional.empty();
        }
    }

    protected void preImageRender(Graphics2D g, int width, int height) {

    }

    protected void postImageRender(Graphics2D g, int width, int height) {

    }
}
