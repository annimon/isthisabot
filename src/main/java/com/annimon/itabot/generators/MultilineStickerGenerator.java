package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.text.AttributedString;
import java.util.Arrays;
import java.util.Comparator;

public abstract class MultilineStickerGenerator extends AbstractStickerGenerator {

    enum VerticalAlign { TOP, CENTER, BOTTOM };

    @Override
    @SuppressWarnings("Duplicates")
    public BufferedImage generateImage(String caption) {
        final var lines = caption.split("\n");
        final int width = stickerWidth();
        final int height = stickerHeight();
        final var img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        final var g = img.createGraphics();

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        preImageRender(g, width, height);

        final var area = bounds();
        final float areaX = (float) area.getX();
        final float areaY = (float) area.getY();
        final float areaWidth = (float) area.getWidth();

        final var frc = g.getFontRenderContext();
        final var font = detectBestFont(frc, lines, area);
        final var textHeight = calculateTextHeight(font.getSize(), lines);
        final var transform = new AffineTransform();
        transform(width, height, transform, area);
        switch (verticalAlign()) {
            case TOP:
                break;
            case CENTER:
                transform.translate(0, (area.getHeight() - textHeight) / 2);
                break;
            case BOTTOM:
                transform.translate(0, area.getHeight() - textHeight);
                break;
        }
        preDrawText(g, textHeight);
        g.transform(transform);

        float posY = areaY;
        float lastFontHeight = 0;
        for (String line : lines) {
            if (line.isEmpty()) {
                posY += lastFontHeight;
                continue;
            }
            final var attrStr = new AttributedString(line);
            attrStr.addAttribute(TextAttribute.FONT, font);
            final var paragraph = attrStr.getIterator();
            final var lineMeasurer = new LineBreakMeasurer(paragraph, frc);
            lineMeasurer.setPosition(paragraph.getBeginIndex());
            final int paragraphEnd = paragraph.getEndIndex();
            lastFontHeight = posY;
            while (lineMeasurer.getPosition() < paragraphEnd) {
                final var textLayout = lineMeasurer.nextLayout(areaWidth);
                final float textWidth = (float) textLayout.getBounds().getWidth();
                final float posX = areaX + (areaWidth - textWidth) / 2f;
                posY += textLayout.getAscent();

                g.setColor(textColor());
                textLayout.draw(g, posX, posY);

                posY += textLayout.getDescent() + textLayout.getLeading();
            }
            lastFontHeight = posY - lastFontHeight;
        }

        postImageRender(g, width, height);

        return img;
    }

    @Override
    protected void preImageRender(Graphics2D g, int width, int height) {
        g.drawImage(templateImage(), 0, 0, null);
    }

    protected abstract Rectangle bounds();

    protected VerticalAlign verticalAlign() {
        return VerticalAlign.CENTER;
    }

    protected void preDrawText(Graphics2D g, float textHeight) {

    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {

    }

    private Font detectBestFont(FontRenderContext frc, String[] lines, Rectangle2D area) {
        // Detect font for widest line
        final var testFont = fontBySize(50);
        final var widestLine = Arrays.stream(lines)
                .max(Comparator.comparingDouble(str -> testFont.getStringBounds(str, frc).getWidth()))
                .orElse(lines[0]);
        final Font widestLineFont = super.detectBestFont(frc, widestLine, (int) area.getWidth());

        // Check vertical bounds
        final int areaHeight = (int) area.getHeight();
        Font font;
        int textHeight;
        int size = widestLineFont.getSize() - 2;
        do {
            font = fontBySize(size);
            textHeight = calculateTextHeight(size, lines) + 2 * textBoundsBorderSize();
            size -= 1;
        } while (size > minFontSize() && (areaHeight <= textHeight));
        return font;
    }

    private int calculateTextHeight(int size, String[] lines) {
        return (int) (lines.length * (size * 1.05));
    }
}
