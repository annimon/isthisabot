package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class FeelsGoodStickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/feels_good.png";
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 25 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }
}
