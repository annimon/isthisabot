package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class Yuu2StickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/yuu2.jpg";
    }

    @Override
    protected int stickerHeight() {
        return 288;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim();
    }

    @Override
    protected int maxFontSize() {
        return 48;
    }
}
