package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class SpidermanStickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/spiderman.jpg";
    }

    @Override
    protected int stickerHeight() {
        return 358;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), 35 + (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected Color strokeColor() {
        return Color.BLACK;
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }
}
