package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import org.jetbrains.annotations.Nullable;

public abstract class TwoCaptionsStickerGenerator extends AbstractStickerGenerator {

    private String id;

    protected String getId() {
        return id;
    }

    protected String setId(String id) {
        this.id = id;
        return id;
    }

    protected abstract String processSecondaryCaption(@Nullable String id);

    protected abstract float secondaryFontSize(Graphics2D g);

    protected abstract void transformSecondaryCaption(final int width,
                                                      final int height,
                                                      AffineTransform transform,
                                                      Rectangle bounds);

    protected Color secondaryTextColor() {
        return textColor();
    }

    protected Color secondaryStrokeColor() {
        return strokeColor();
    }

    @Override
    protected void postImageRender(Graphics2D g, int width, int height) {
        final var frc = g.getFontRenderContext();
        final float fontSize = secondaryFontSize(g);
        final Font font = fontBySize((int) fontSize);

        final var caption = processSecondaryCaption(id);
        final var textLayout = new TextLayout(caption, font, frc);
        final var transform = new AffineTransform();
        final Shape outline = textLayout.getOutline(null);
        final Rectangle bounds = outline.getBounds();
        transformSecondaryCaption(width, height, transform, bounds);
        final var oldTransform = g.getTransform();
        g.transform(transform);
        final float strokeSize = strokeSizeByFontSize(fontSize);
        if (strokeSize > 0f) {
            final Stroke oldStroke = g.getStroke();
            g.setColor(secondaryStrokeColor());
            g.setStroke(new BasicStroke(strokeSize));
            g.draw(outline);
            g.setStroke(oldStroke);
        }
        g.setColor(secondaryTextColor());
        textLayout.draw(g, 0, 0);
        g.setTransform(oldTransform);
    }
}
