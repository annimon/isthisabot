package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KomiStickerGenerator extends MultilineStickerGenerator {

    private static final Logger log = LoggerFactory.getLogger(KomiStickerGenerator.class);

    private static Font ttfFont;

    protected String templateResource() {
        return "/komi-san.png";
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected Rectangle bounds() {
        return new Rectangle(84, 313, 174, 187);
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.rotate(-0.006);
    }

    @Override
    protected Font fontBySize(int size) {
        if (ttfFont == null) {
            try (var is = getClass().getResource("/anime-ace.ttf").openStream()) {
                ttfFont = Font.createFont(Font.TRUETYPE_FONT, is);
            } catch (IOException | FontFormatException ex) {
                log.error("KomiStickerGenerator", ex);
            }
        }
        if (ttfFont == null) {
            return super.fontBySize(size);
        }
        return ttfFont.deriveFont((float) size);
    }
}
