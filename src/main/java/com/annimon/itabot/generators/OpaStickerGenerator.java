package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class OpaStickerGenerator extends AbstractStickerGenerator {

    private static final Color TEXT_COLOR = new Color(0xFFFA2E0F);

    @Override
    protected String templateResource() {
        return "/opa.png";
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 8 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 0f;
    }

    @Override
    protected Color textColor() {
        return TEXT_COLOR;
    }
}
