package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class ComeStickerGenerator extends AbstractStickerGenerator {

    private final Color TEXT_COLOR = new Color(0xFF87090f);

    @Override
    protected String templateResource() {
        return "/come.png";
    }

    @Override
    protected int stickerHeight() {
        return 486;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 10 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    protected Color textColor() {
        return TEXT_COLOR;
    }

    protected Color strokeColor() {
        return Color.WHITE;
    }
}
