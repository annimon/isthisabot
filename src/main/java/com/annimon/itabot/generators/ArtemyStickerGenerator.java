package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class ArtemyStickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/artemy.jpg";
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 20 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }

    @Override
    protected Color strokeColor() {
        return Color.BLACK;
    }
}
