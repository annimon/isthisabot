package com.annimon.itabot.generators;

import java.awt.*;

public class DogeStickerGenerator extends MultilineStickerGenerator {

    protected String templateResource() {
        return "/doge.png";
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected Rectangle bounds() {
        final int pad = 20;
        final int startY = 190;
        return new Rectangle(pad, startY, 512 - pad * 2, 512 - startY - pad);
    }

    @Override
    protected VerticalAlign verticalAlign() {
        return VerticalAlign.BOTTOM;
    }
}
