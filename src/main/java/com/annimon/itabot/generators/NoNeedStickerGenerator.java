package com.annimon.itabot.generators;

import java.awt.*;
import org.jetbrains.annotations.Nullable;

public class NoNeedStickerGenerator extends NeedStickerGenerator {

    @Override
    protected String templateResource() {
        return "/noneed.png";
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        if (id == null || id.equalsIgnoreCase("не нужно")) {
            return "НЕ НУЖНО БЫЛО";
        }
        if (id.equalsIgnoreCase("no need")) {
            return "NO NEED TO";
        }
        return id.toUpperCase();
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        return 46;
    }
}
