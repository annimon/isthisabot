package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;
import org.jetbrains.annotations.Nullable;

public class JohanStickerGenerator extends TwoCaptionsStickerGenerator {

    @Override
    protected String templateResource() {
        return "/johan.jpg";
    }

    @Override
    protected int actualStickerWidth() {
        return 500;
    }

    @Override
    protected int stickerHeight() {
        return 333;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 15 - (bounds.height / 2f));
    }

    @Override
    protected void transformSecondaryCaption(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), 35 + (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        super.setId(id);
        return caption.trim().toUpperCase();
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        if (id != null && id.equalsIgnoreCase("до утра")) {
            return "ЧТОБЫ ДО УТРА";
        }
        if (id != null && id.equalsIgnoreCase("до вечера")) {
            return "ЧТОБЫ ДО ВЕЧЕРА";
        }
        return "ЧТОБЫ ДО ОБЕДА";
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        return 44;
    }

    @Override
    protected Color strokeColor() {
        return Color.BLACK;
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }

    @Override
    protected int minFontSize() {
        return 25;
    }

    @Override
    protected int maxFontSize() {
        return 54;
    }
}
