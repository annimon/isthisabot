package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import org.jetbrains.annotations.Nullable;

public class ShowStickerGenerator extends TwoCaptionsStickerGenerator {

    @Override
    protected String templateResource() {
        return "/showme.png";
    }

    @Override
    protected int stickerWidth() {
        return 512;
    }

    @Override
    protected int stickerHeight() {
        return 496;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width - 86 - bounds.width / 2f, height / 2f - 95 + 1.25f * bounds.height);
    }

    @Override
    protected void transformSecondaryCaption(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width - 86 - bounds.width / 2f, height / 2f - 90 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        super.setId(id);
        return caption.trim().toUpperCase();
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        if (id == null) {
            return "ПОКАЗЫВАЙ";
        }
        return id.toUpperCase();
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        return "show".equalsIgnoreCase(getId()) ? 26 : 18;
    }

    @Override
    protected int minFontSize() {
        return 8;
    }

    @Override
    protected int maxFontSize() {
        return 32;
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 0;
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return super.detectBestFont(frc, caption, 128);
    }
}
