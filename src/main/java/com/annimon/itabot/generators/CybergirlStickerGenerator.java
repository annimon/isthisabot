package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class CybergirlStickerGenerator extends MultilineStickerGenerator {

    protected String templateResource() {
        return "/cybergirl.png";
    }

    @Override
    protected int stickerWidth() {
        return 398;
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim();
    }

    @Override
    protected Rectangle bounds() {
        return new Rectangle(88, 10, 228, 148);
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.rotate(0.06);
    }

    @Override
    protected void preImageRender(Graphics2D g, int width, int height) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);
    }

    @Override
    protected void postImageRender(Graphics2D g, int width, int height) {
        g.setTransform(new AffineTransform());
        g.drawImage(templateImage(), 0, 0, null);
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 0;
    }
}
