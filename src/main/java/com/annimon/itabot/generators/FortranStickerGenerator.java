package com.annimon.itabot.generators;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class FortranStickerGenerator extends AbstractStickerGenerator {

    @Override
    protected String templateResource() {
        return "/fortran.png";
    }

    @Override
    protected int stickerWidth() {
        return 512;
    }

    @Override
    protected int stickerHeight() {
        return 345;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(165 - (bounds.width / 2f), height / 2f - 25 - (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected int minFontSize() {
        return 25;
    }

    @Override
    protected int maxFontSize() {
        return 64;
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return super.detectBestFont(frc, caption, 330);
    }
}
