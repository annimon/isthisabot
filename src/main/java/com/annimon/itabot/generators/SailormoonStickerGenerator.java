package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;
import org.jetbrains.annotations.Nullable;

public class SailormoonStickerGenerator extends TwoCaptionsStickerGenerator {

    @Override
    protected String templateResource() {
        return "/sailormoon.jpg";
    }

    @Override
    protected int stickerHeight() {
        return 384;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), height - 10 - (bounds.height / 2f));
    }

    @Override
    protected void transformSecondaryCaption(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), 35 + (bounds.height / 2f));
    }

    @Override
    public String processCaption(String id, String caption) {
        super.setId(id);
        return caption.trim().toUpperCase();
    }

    @Override
    protected String processSecondaryCaption(@Nullable String id) {
        if (id != null && (id.equalsIgnoreCase("sailormoon")
                || id.equalsIgnoreCase("moon prism"))) {
            return "MOON PRISM";
        }
        return "ЛУННАЯ ПРИЗМА";
    }

    @Override
    protected float secondaryFontSize(Graphics2D g) {
        return 44;
    }

    @Override
    protected Color strokeColor() {
        return Color.BLACK;
    }

    @Override
    protected Color textColor() {
        return Color.WHITE;
    }

    @Override
    protected int minFontSize() {
        return 25;
    }

    @Override
    protected int maxFontSize() {
        return 54;
    }
}
