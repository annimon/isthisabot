package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class IsThisAPigeonStickerGenerator extends AbstractStickerGenerator {

    @Override
    public String processCaption(String id, String caption) {
        String result = caption.trim().toUpperCase();
        if (result.endsWith("?")) {
            return result;
        } else {
            return result + "?";
        }
    }

    @Override
    protected String templateResource() {
        return "/is-this-a-pigeon.png";
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f - (bounds.width / 2f), 75 + (bounds.height / 2f));
    }
}
