package com.annimon.itabot.generators;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class CatStickerGenerator extends AbstractStickerGenerator {

    @Override
    public String processCaption(String id, String caption) {
        return caption.trim().toUpperCase();
    }

    @Override
    protected String templateResource() {
        return "/cat.png";
    }

    @Override
    protected float strokeSizeByFontSize(float fontSize) {
        return 0f;
    }

    @Override
    protected void transform(int width, int height, AffineTransform transform, Rectangle bounds) {
        transform.translate(width / 2f + 10 - (bounds.width / 2f), 95 + (bounds.height / 2f));
    }

    @Override
    protected Font detectBestFont(FontRenderContext frc, String caption, int maxWidth) {
        return super.detectBestFont(frc, caption, 470);
    }
}
