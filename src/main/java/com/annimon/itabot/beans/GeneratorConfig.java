package com.annimon.itabot.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeneratorConfig {

    @JsonProperty(value = "classname", required = true)
    private String className;

    @JsonProperty(value = "public", defaultValue = "false")
    private boolean isPublic;

    @JsonProperty(required = true)
    private String[] aliases;

    public GeneratorConfig() {
    }

    public GeneratorConfig(String className, boolean isPublic, String[] aliases) {
        this.className = className;
        this.isPublic = isPublic;
        this.aliases = aliases;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public String[] getAliases() {
        return aliases;
    }

    public void setAliases(String[] aliases) {
        this.aliases = aliases;
    }

    @Override
    public String toString() {
        return "Generator{" +
                "className='" + className + '\'' +
                ", isPublic=" + isPublic +
                ", aliases=" + Arrays.toString(aliases) +
                '}';
    }
}
