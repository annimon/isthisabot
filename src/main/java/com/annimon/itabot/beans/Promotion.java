package com.annimon.itabot.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Promotion {

    @NotNull
    @JsonProperty(required = true)
    public String title;

    @Nullable
    public String lang;

    @Nullable
    public String content;

    @Nullable
    public String photoId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Promotion promotion = (Promotion) o;
        return title.equals(promotion.title) &&
                Objects.equals(lang, promotion.lang) &&
                Objects.equals(content, promotion.content) &&
                Objects.equals(photoId, promotion.photoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, lang, content, photoId);
    }
}
