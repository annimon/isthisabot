package com.annimon.itabot.beans;

import com.annimon.itabot.generators.StickerGenerator;

public class StickerPreprocessStatus {

    public static final StickerPreprocessStatus FAIL = new StickerPreprocessStatus(true, null, null);

    private final boolean fail;
    private final StickerGenerator generator;
    private final String caption;

    public StickerPreprocessStatus(boolean fail, StickerGenerator generator, String caption) {
        this.fail = fail;
        this.generator = generator;
        this.caption = caption;
    }

    public boolean isFail() {
        return fail;
    }

    public StickerGenerator getGenerator() {
        return generator;
    }

    public String getCaption() {
        return caption;
    }



}
