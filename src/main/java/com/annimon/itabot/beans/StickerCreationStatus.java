package com.annimon.itabot.beans;

public class StickerCreationStatus {

    public static final StickerCreationStatus FAIL = new StickerCreationStatus(true, false, false, null, null);
    public static final StickerCreationStatus NEED_DOT = new StickerCreationStatus(true, true, false, null, null);

    public static StickerCreationStatus with(String fileId, String caption, boolean usedCache) {
        return new StickerCreationStatus(false, false, usedCache, fileId, caption);
    }

    private final boolean fail;
    private final boolean needDot;
    private final boolean usedCache;
    private final String fileId;
    private final String caption;

    private StickerCreationStatus(boolean fail, boolean needDot, boolean usedCache, String fileId, String caption) {
        this.fail = fail;
        this.needDot = needDot;
        this.usedCache = usedCache;
        this.fileId = fileId;
        this.caption = caption;
    }

    public boolean isFail() {
        return fail;
    }

    public boolean isNeedDot() {
        return needDot;
    }

    public boolean isUsedCache() {
        return usedCache;
    }

    public String getFileId() {
        return fileId;
    }

    public String getCaption() {
        return caption;
    }
}
