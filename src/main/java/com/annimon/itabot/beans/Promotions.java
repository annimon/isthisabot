package com.annimon.itabot.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.jetbrains.annotations.NotNull;

public class Promotions {

    @NotNull
    @JsonProperty(required = true)
    public List<Promotion> promotions;

    public Promotions() {
        this.promotions = Collections.emptyList();
    }

    public Promotions(@NotNull List<Promotion> promotions) {
        this.promotions = new CopyOnWriteArrayList<>(promotions);
    }
}
