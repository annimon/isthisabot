package com.annimon.itabot.beans;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItaBotConfig {

    @JsonProperty(required = true)
    private String token;

    @JsonProperty(required = true)
    private String username;

    @JsonProperty(required = true)
    private String channel;

    private Long creatorId = 0L;

    public ItaBotConfig() {
    }

    public ItaBotConfig(String token, String username, String channel) {
        this.token = token;
        this.username = username;
        this.channel = channel;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "Bot{" +
                "token='" + token + '\'' +
                ", username='" + username + '\'' +
                ", channel='" + channel + '\'' +
                ", creatorId='" + creatorId + '\'' +
                '}';
    }
}
