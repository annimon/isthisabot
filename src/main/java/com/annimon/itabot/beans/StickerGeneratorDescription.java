package com.annimon.itabot.beans;

import java.util.Map;

public class StickerGeneratorDescription {

    private final Map<String, String> descriptions;
    private final String fallbackDescription;

    public StickerGeneratorDescription(Map<String, String> descriptions) {
        this.descriptions = descriptions;
        final String desc;
        if (descriptions.containsKey("en")) {
            desc = descriptions.get("en");
        } else if (!descriptions.isEmpty()) {
            desc = descriptions.values().iterator().next();
        } else {
            desc = "";
        }
        this.fallbackDescription = desc;
    }

    public String getDescription(String locale) {
        return descriptions.getOrDefault(locale, fallbackDescription);
    }
}
